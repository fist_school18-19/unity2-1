﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bullet : NetworkBehaviour {
    public int damage;
    public int speed;
    [SyncVar] Vector3 syncPos;
    [SerializeField][SyncVar] float lifeTime;

    void Start()
    {
        syncPos = transform.position;
    }

    void FixedUpdate()
    {
        if (isServer)
        {
            transform.Translate(
                Vector3.forward * speed * Time.deltaTime
            );
            syncPos = transform.position;
        }
        if (isClient)
        {
            transform.position = Vector3.Lerp(
                transform.position,
                syncPos,
                5 * Time.deltaTime
                );
        }
        lifeTime -= Time.fixedDeltaTime;
        if (lifeTime <= 0)
            NetworkServer.Destroy(gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        HP hp;
        if (hp = collider.GetComponentInParent<HP>())
            // HP, component, obj -> bool => true
            // null           -> bool => false
        {
            hp.GetDamage(damage);
        }
        Destroy(gameObject);
    }
}
