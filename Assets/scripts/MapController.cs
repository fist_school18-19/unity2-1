﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

class Marker
{
    public GameObject owner;
    public Image image;
    public Marker(GameObject go, Sprite s)
    {
        this.owner = go;
        this.image = Image.Instantiate(MapController.GetInstance().markerPrefab);
        this.image.sprite = s;
    }
}

public class MapController : MonoBehaviour {

    public Image markerPrefab;
    static List<Marker> markers = new List<Marker>();
    [HideInInspector] public Transform playerPos;

    [Range(0.1f, 5)][SerializeField] float scale;

    public Text localHealthText;
    public Image localHealthBar;
    public RectTransform localBuildingPanel;
    public RectTransform selector;
    public Text hint;

    private static MapController _script;
    public static MapController GetInstance() { return _script; }

	void Awake ()
    {
        if (MapController.GetInstance() == null)
            _script = this;
	}
	
	// Update is called once per frame
	void Update () {
        foreach (Marker m in markers)
        {
            Vector3 rel = Vector3.zero;
            rel.x = m.owner.transform.position.x - playerPos.position.x;
            rel.y = m.owner.transform.position.z - playerPos.position.z;

            rel *= scale;
            rel = Vector3.ClampMagnitude(rel, 87f);

            Vector3 tmp = Vector3.zero;
            tmp.z = -playerPos.transform.rotation.eulerAngles.y;
            m.image.transform.localRotation = Quaternion.Euler(tmp);

            m.image.transform.localPosition = rel;
        }
        Vector3 tmp2 = Vector3.zero;
        tmp2.z = playerPos.transform.rotation.eulerAngles.y;
        transform.localRotation = Quaternion.Euler(tmp2);
	}

    public static void RegisterObject(GameObject owner, Sprite sprite)
    {
        Marker m = new Marker(owner, sprite);
        markers.Add(m);
        m.image.transform.SetParent(MapController.GetInstance().transform);
    }
    public static void RemoveObject(GameObject go)
    {
        Marker tmp = markers.First(m => m.owner == go); // linq
        Destroy(tmp.image);
        markers.Remove(tmp);
    }
}
