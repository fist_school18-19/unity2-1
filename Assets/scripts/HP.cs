﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class HP : NetworkBehaviour {
    [SerializeField][Range(0, 100)] int health;
    Text healthText;
    Image healthBar;

    void Start()
    {
        if (isLocalPlayer)
        {
            healthBar = MapController.GetInstance()
                .localHealthBar;
            healthText = MapController.GetInstance()
                .localHealthText;
        }
    }

    public void GetDamage(int value)
    {
        health -= value;
        if (health <= 0) NetworkServer.Destroy(gameObject);
        if (healthText) healthText.text = health.ToString();
        if (healthBar) healthBar.fillAmount = (health / 100f);
    }

}
