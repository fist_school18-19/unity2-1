﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CarController : MonoBehaviour {

    public float maxAngle = 30f;
    public float maxTorque = 25f;
    public float maxBrake = 50f;

    public List<WheelCollider> driveWheels;
    public List<WheelCollider> steerWheels;

    public List<Animator> brakeLigts;
    public List<Animator> leftTurnLigts;
    public List<Animator> rightTurnLigts;

	void Start ()
    {
	}

    void FixedUpdate()
    {
        float angle = maxAngle * Input.GetAxis("Horizontal");
        float torque = maxTorque * Input.GetAxis("Vertical");

        if (Input.GetAxis("Vertical") < 0)
        {
            foreach (Animator a in brakeLigts)
                a.SetBool("isOn", true);
        }
        else
        {
            foreach (Animator a in brakeLigts)
                a.SetBool("isOn", false);
        }

        if (Input.GetAxis("Horizontal") < 0) {
            foreach (Animator a in leftTurnLigts)
                a.SetBool("isBlink", true);
        } else if (Input.GetAxis("Horizontal") > 0) {
            foreach (Animator a in rightTurnLigts)
                a.SetBool("isBlink", true);
        } else {
            foreach (Animator a in rightTurnLigts)
                a.SetBool("isBlink", false);
            foreach (Animator a in leftTurnLigts)
                a.SetBool("isBlink", false);
        }

        foreach (WheelCollider wc in steerWheels)
        {
            // поворот рулевых колес
            wc.steerAngle = angle;

            Vector3 pos;
            Quaternion rot;
            wc.GetWorldPose(out pos, out rot);

            wc.transform.GetChild(0).transform.position = pos;
            wc.transform.GetChild(0).transform.rotation = rot;
        }

        foreach (WheelCollider wc in driveWheels)
        {
            // вращение приводных колес
            wc.motorTorque = torque;
            wc.brakeTorque = 0;

            // update meshes:
            Vector3 pos;
            Quaternion rot;
            wc.GetWorldPose(out pos, out rot);

            wc.transform.GetChild(0).transform.position = pos;
            wc.transform.GetChild(0).transform.rotation = rot;
        }
    }
}
