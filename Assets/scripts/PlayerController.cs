﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : NetworkBehaviour
{
    [SyncVar]
    Vector3 playerPos;
    [SyncVar]
    Quaternion playerRot;
    [SyncVar]
    float fSpeed;
    [SyncVar]
    float sSpeed;
    [SyncVar]
    bool jump;


    [SerializeField]
    Transform shootPoint;
    [SerializeField]
    GameObject bulletPrefab;
    [SerializeField]
    [Range(1, 30)]
    float lerpSpeed = 1;

    [Header("Building mode")]
    [SerializeField] GameObject marker;
    [SerializeField] GameObject[] buildingsPrefabs;
    int selectedBuilding = 0;

    Animator anim;
    Camera cam;

    Item itemToInteract;

    bool buildingMode = false;
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        cam = GetComponentInChildren<Camera>();
        if (isLocalPlayer)
        {
            MapController.GetInstance().playerPos = transform;
            EnablePlayer(true);
        }
    }
    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            RaycastHit hit = new RaycastHit();
            Physics.Raycast(
                transform.position,
                -transform.up,
                out hit,
                1.2f
                );

            print(hit.collider == null);
            jump = !hit.collider;

            if (buildingMode)
            {
                hit = new RaycastHit();
                Physics.Raycast(cam.transform.position, cam.transform.forward,
                    out hit, 5f);
                if (hit.collider && hit.collider.tag == "Building")
                {
                    marker.transform.position = hit.point;
                    marker.GetComponent<MeshRenderer>().enabled = true;
                } else { marker.GetComponent<MeshRenderer>().enabled = false; }
            }

        }
        LerpTransform();
        TransmitTransform();
    }
    void Update()
    {
        if (!isLocalPlayer) return;

        if (!buildingMode)
        {
            if (Input.GetMouseButtonDown(0))
            {
                CmdInstBullet();
                anim.SetBool("shoot", true);
            }
            if (Input.GetMouseButtonUp(0)) anim.SetBool("shoot", false);
            if (Input.GetMouseButtonDown(1)) anim.SetBool("aim", true);
            if (Input.GetMouseButtonUp(1)) anim.SetBool("aim", false);
        }
        else
        {
            if (Input.GetMouseButtonDown(0) &&
                marker.GetComponent<MeshRenderer>().enabled)
            {
                GameObject.Instantiate(
                    buildingsPrefabs[selectedBuilding],
                    marker.transform.position,
                    marker.transform.rotation,
                    null
                    );
            }
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            buildingMode = !buildingMode;
            marker.GetComponent<MeshRenderer>().enabled = buildingMode;
            MapController.GetInstance()
                .localBuildingPanel.gameObject.SetActive(buildingMode);

        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0) {
            selectedBuilding -= System.Math
                .Sign(Input.GetAxis("Mouse ScrollWheel"));
            int k = MapController.GetInstance().localBuildingPanel.childCount;
            selectedBuilding = (k + selectedBuilding) % k;
            MapController.GetInstance().selector
                .SetParent(MapController.GetInstance()
                    .localBuildingPanel
                    .GetChild(selectedBuilding));
            MapController.GetInstance().selector.localPosition = Vector3.zero;
        }

        fSpeed = Input.GetAxis("Vertical");
        sSpeed = Input.GetAxis("Horizontal");

        anim.SetFloat("fspeed", fSpeed);
        anim.SetFloat("sspeed", sSpeed);

        // interact feature:
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(
            cam.transform.position,
            cam.transform.forward,
            out hit,
            2.0f
            );
        //print(hit.collider.gameObject.name);
        if (hit.collider && hit.collider.GetComponent<Item>())
        {
            itemToInteract = hit.collider.GetComponent<Item>();
            MapController.GetInstance()
                .hint.gameObject.SetActive(true);
            MapController.GetInstance().hint.text =
                "Use " + hit.collider.gameObject.name;
        }
        
        else
        {
            itemToInteract = null;
            MapController.GetInstance()
                .hint.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.E) && itemToInteract)
            itemToInteract.Use();
    }
    void LerpTransform()
    {
        if (isLocalPlayer) return;

        transform.position = Vector3.Lerp(transform.position, playerPos, Time.deltaTime * lerpSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, playerRot, Time.deltaTime * lerpSpeed);

        float tmpF = Mathf.Lerp(
            anim.GetFloat("fspeed"),
            fSpeed,
            Time.deltaTime * lerpSpeed
        );
        anim.SetFloat("fspeed", tmpF);

        float tmpS = Mathf.Lerp(
            anim.GetFloat("sspeed"),
            sSpeed,
            Time.deltaTime * lerpSpeed
        );
        anim.SetFloat("sspeed", tmpS);
        anim.SetBool("jump", jump);
    }
    [Client]
    void TransmitTransform()
    {
        if (!isLocalPlayer) return;
        CmdSyncTransform(transform.position, transform.rotation);
        CmdSyncAnimator(
            anim.GetFloat("fspeed"),
            anim.GetFloat("sspeed"),
            anim.GetBool("jump")
        );
    }
    [Command]
    void CmdSyncTransform(Vector3 pos, Quaternion rot)
    {
        playerPos = pos;
        playerRot = rot;
    }
    [Command]
    void CmdSyncAnimator(float f, float s, bool j)
    {
        fSpeed = f;
        sSpeed = s;
        jump = j;
    }
    [Command]
    void CmdInstBullet()
    {
        GameObject b = GameObject.Instantiate(
                bulletPrefab,
                shootPoint.position,
                shootPoint.rotation
                );
        NetworkServer.Spawn(b);
    }
    void EnablePlayer(bool value)
    {
        GetComponent<CharacterController>().enabled = value;
        GetComponent<FirstPersonController>().enabled = value;

        GetComponentInChildren<Camera>().enabled = value;
        GetComponentInChildren<AudioListener>().enabled = value;
    }
}
