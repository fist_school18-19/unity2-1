﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Case : Item {
    public override void Use()
    {
        base.Use();
        foreach (Transform item in transform)
        {
            item.gameObject.SetActive(true);
        }
        Destroy(this);
    }
}
