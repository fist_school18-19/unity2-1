﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : Collectable {
    public override void Use()
    {
        base.Use();
        if (stackable && count > 1) count--;
        else Destroy(gameObject);

    }
}
