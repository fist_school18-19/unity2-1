﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

	// Use this for initialization
    public static Inventory instance;
	void Start () {
        if (!instance) instance = this;
	}
    List<Collectable> items = new List<Collectable>();

    public void AddItem(Collectable item)
    {
        items.Add(item);
    }
    public void RemoveItem(Collectable item)
    {
        // check if stackable
        items.Remove(item);
    }
}
