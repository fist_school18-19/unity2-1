﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : Collectable {
    [Range(0, 100)] int health;
    public override void Use()
    {
        base.Use();
        // eqiup or unequip me
    }
    public virtual void Repair() { health = 100; }
}
