﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : Weapon {
    public RangeWeaponsTypes AmmoType;
    public float range;
    public int ammoCapacity;
    public int currentAmmo;
    public float reloadTime;

    public void Reload()
    {
        // if i have aprop ammo
        // anim
        currentAmmo = ammoCapacity;
    }
}
