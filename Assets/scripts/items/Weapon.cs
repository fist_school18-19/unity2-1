﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Equipment {
    public int Damage;
    [Range (0.1f, 1)] float DamageMultipier = 1f;

    public override void Repair()
    {
        base.Repair();
        DamageMultipier = 1f;
    }
}
