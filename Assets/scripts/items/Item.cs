﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
    Animator anim;
    void Start() { anim = GetComponent<Animator>(); }
    public virtual void Use()
    {
        if (anim) anim.SetTrigger("use");
    }
}
