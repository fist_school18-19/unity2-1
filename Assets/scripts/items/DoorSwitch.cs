﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : Item {

    [SerializeField] GameObject door;

    public override void Use()
    {
        base.Use();
        door.transform.rotation =
            Quaternion.EulerAngles(
                0,
                door.transform.rotation.eulerAngles.y + 45,
                0);
    }
}
