﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Equipment {
    [Range(0.0f, 1f)] float DamageAbsorbation;
    // dmg = 20, dabs = 0.33, armor.h -= 6.6,
    // player -= dmg*(1-dabs) -= 3.4

    public override void Repair()
    {
        base.Repair();
        DamageAbsorbation = 1f;
    }
}
