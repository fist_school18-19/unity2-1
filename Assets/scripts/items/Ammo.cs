﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : Consumable {
    public int damage;
    public float velocity;
    public RangeWeaponsTypes WeaponType;
}

enum RangeWeaponsTypes
{
    Pistol,
    Shotgun,
    Automatic,
    Rifle,
    Energy,
    RocketLauncher
}

