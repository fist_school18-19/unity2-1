﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour {
    public int id;
    public string name;
    public Sprite icon;

    public bool stackable = false;
    public int stackMaxSize = 0;
    public int count = 0;

    public GameObject prefab;

    public virtual void Use()
    {
        // visualize it!
    }
    public virtual void Drop()
    {
        Inventory.instance.RemoveItem(this);
    }
}
