﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarEnterExit : MonoBehaviour {
    bool inZone = false;
    bool inCar = false;
    GameObject player;
	void Update () {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (inCar)
            {
                // get out of my car now
                player.GetComponentInChildren<Camera>().enabled = true;
                player.SetActive(true);

                gameObject.GetComponentInChildren<Camera>().enabled = false;
                gameObject.GetComponent<CarController>().enabled = false;

                player.transform.SetParent(null);
                inCar = false;
            }
            else if (inZone)
            {
                player.GetComponentInChildren<Camera>().enabled = false;
                player.SetActive(false);

                gameObject.GetComponentInChildren<Camera>().enabled = true;
                gameObject.GetComponent<CarController>().enabled = true;

                player.transform.SetParent(gameObject.transform);
                inCar = true;
            }
        }   
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") inZone = true;
        {
            player = other.gameObject;
        }

    }
    void OnTriggerExit(Collider other) { if (other.gameObject.tag == "Player") inZone = false; }
}
