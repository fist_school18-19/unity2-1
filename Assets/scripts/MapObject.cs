﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapObject : MonoBehaviour {
    
    public Sprite marker;
	void Start ()
    {
        MapController.RegisterObject(gameObject, marker);
    }
    void OnDestroy()
    {
        MapController.RemoveObject(gameObject);
    }
}
